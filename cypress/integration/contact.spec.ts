const pageUrl = Cypress.env('BASE_URL') + '/contact';

describe('Contact page integration tests', () => {
  beforeEach(() => {
    cy.server();
    cy.route('GET', '**/api/contactos', 'fx:contacts/getContacts').as('getContacts');
    cy.route('POST', '**/api/contactos', {}).as('postContact');

    cy.visit(pageUrl);

    cy.get('app-contact').as('contactComponent');
  });

  it('should add contact', () => {
    cy.wait('@getContacts');

    cy.route('GET', '**/api/contactos', 'fx:contacts/getContactsUpdated')
      .as('getContactsUpdated');

    cy.get('@contactComponent')
      .find('button[type="submit"]')
      .as('submitButton');

    cy.get('@submitButton')
      .should('be.disabled');

    cy.fixture('contacts/newContact').then(newContact => {
      cy.get('@contactComponent')
        .find('input[formcontrolname="name"]')
        .type(newContact.nome);

      cy.get('@contactComponent')
        .find('input[formcontrolname="email"]')
        .type(newContact.email);

      cy.get('@contactComponent')
        .find('input[formcontrolname="message"]')
        .type(newContact.msg);
    });

    cy.get('@submitButton')
      .should('not.be.disabled');

    cy.get('@submitButton')
      .click();

    cy.wait('@getContactsUpdated');

    cy.fixture('contacts/newContact').then(newContact => {
      cy.get('@contactComponent')
        .find('table')
        .should('contain', newContact.nome)
        .should('contain', newContact.email)
        .should('contain', newContact.msg);
    });
  });
});

describe('Contact page integration tests', () => {

});
