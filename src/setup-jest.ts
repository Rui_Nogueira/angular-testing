import 'jest-preset-angular';
import { environment } from '@env/environment';
import { environment as testEnvironment } from '@env/environment.test';

// Override default environment variables with test environment variables
for (const [key, value] of Object.entries(testEnvironment)) {
  environment[key] = value;
}
