export interface Contact {
  id: number;
  nome: string;
  email: string;
  msg: string;
}

export type GetContactDto = Contact;

export type AddContactContext = Pick<Contact, 'nome' | 'email' | 'msg'>;
