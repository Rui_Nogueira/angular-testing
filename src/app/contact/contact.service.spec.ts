import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { ContactService } from './contact.service';
import { environment } from '@env/environment';
import { Contact, GetContactDto, AddContactContext } from '@app/shared/models/contact.model';

describe('ContactService', () => {
  let httpTestingController: HttpTestingController;
  let service: ContactService;
  let contactsMock: Contact[];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContactService],
      imports: [HttpClientTestingModule]
    });

    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(ContactService);

    contactsMock = [{ id: 1, email: 'email@email.com', nome: 'Test Name', msg: 'Test msg' }];
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get the contacts', () => {
    const expectedResult: GetContactDto[] = contactsMock;
    service.getContacts().subscribe(result => {
      expect(result).toEqual(expectedResult);
    });

    const req = httpTestingController.expectOne(environment.apiUrl + '/contactos');

    expect(req.request.method).toEqual('GET');

    req.flush(contactsMock);
  });

  it('should add a contact', () => {
    const contact: AddContactContext = {
      nome: contactsMock[0].nome,
      email: contactsMock[0].email,
      msg: contactsMock[0].msg,
    };

    service.addContact(contact).subscribe();

    const req = httpTestingController.expectOne(environment.apiUrl + '/contactos');

    expect(req.request.method).toEqual('POST');
  });
});
