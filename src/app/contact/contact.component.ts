import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactService } from './contact.service';
import { Contact, AddContactContext } from '@app/shared/models/contact.model';
import { flatMap } from 'rxjs/operators';

interface ContactForm {
  name: string;
  email: string;
  message: string;
}

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contacts!: Contact[];
  contactForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private contactService: ContactService
  ) {
    this.contactForm = this.createForm();
  }

  ngOnInit() {
    this.contactService
      .getContacts()
      .subscribe(
        contacts => this.contacts = contacts
      );
  }

  get contactFormValue() {
    return this.contactForm.value as ContactForm;
  }

  handleAddContact() {
    const contactDto: AddContactContext = {
      nome: this.contactFormValue.name,
      email: this.contactFormValue.email,
      msg: this.contactFormValue.message
    };

    this.contactService
      .addContact(contactDto)
      .pipe(
        flatMap(() => this.contactService.getContacts())
      )
      .subscribe(
        contacts => this.contacts = contacts
      );
  }

  private createForm() {
    return this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      message: ['', Validators.required]
    });
  }
}
