import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ContactComponent } from './contact.component';
import { ContactService } from './contact.service';
import { createContactsMock } from './contact.mock';
import { ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { AddContactContext } from '@app/shared/models/contact.model';

class ContactServiceMock {
  contacts = createContactsMock();

  getContacts = jest.fn(() => of(this.contacts));
  addContact = jest.fn((contact: AddContactContext) => {
    this.contacts.push({ id: this.contacts.length, ...contact });
    return of({});
  });
}

describe('ContactComponent', () => {
  let component: ContactComponent;
  let fixture: ComponentFixture<ContactComponent>;
  const contactServiceMock = new ContactServiceMock();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [ContactComponent],
      providers: [{ provide: ContactService, useValue: contactServiceMock }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get the contacts', () => {
    expect(contactServiceMock.getContacts).toHaveBeenCalledTimes(1);
    const expectedResult = createContactsMock();
    expect(component.contacts).toEqual(expectedResult);
  });

  it('should add a contact', () => {
    const newContactMock: AddContactContext = {
      nome: 'Test Name',
      email: 'Test Email',
      msg: 'Test Message'
    };

    expect(component.contactForm.valid).toBeFalsy();
    component.contactForm.controls.name.setValue(newContactMock.nome);
    component.contactForm.controls.email.setValue(newContactMock.email);
    component.contactForm.controls.message.setValue(newContactMock.msg);
    expect(component.contactForm.valid).toBeTruthy();

    const expectedResult = contactServiceMock.contacts.concat({ id: contactServiceMock.contacts.length, ...newContactMock });
    component.handleAddContact();

    expect(contactServiceMock.addContact).toHaveBeenCalledTimes(1);
    expect(contactServiceMock.getContacts).toHaveBeenCalledTimes(2);
    expect(component.contacts).toEqual(expectedResult);
  });
});
