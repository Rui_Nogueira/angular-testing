import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { GetContactDto, AddContactContext } from '@app/shared/models/contact.model';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private static readonly API_URL = environment.apiUrl;
  private static readonly CONTACTS_URL = ContactService.API_URL + '/contactos';

  constructor(private httpClient: HttpClient) { }

  getContacts() {
    return this.httpClient.get<GetContactDto[]>(ContactService.CONTACTS_URL);
  }

  addContact(contact: AddContactContext) {
    return this.httpClient.post(ContactService.CONTACTS_URL, contact);
  }
}
