import { Contact } from '@app/shared/models/contact.model';

export function createContactsMock(): Contact[] {
  return [{ id: 0, email: 'email@email.com', nome: 'Test Name', msg: 'Test msg' }];
}
