module.exports = {
  preset: "jest-preset-angular",
  setupFilesAfterEnv: ["<rootDir>/src/setup-jest.ts"],
  testRegex: "src[\\/].*\\.spec\\.ts$",
  testMatch: null,
  moduleNameMapper: {
    "@app/(.*)": "<rootDir>/src/app/$1",
    "@env/(.*)": "<rootDir>/src/environments/$1"
  },
  collectCoverage: true,
  coverageThreshold: {
    global: {
      branches: 90,
      functions: 90,
      lines: 90,
      statements: 90
    }
  },
  coverageReporters: ["json", "html"]
};
